package com.example.livedata.application

import android.app.Application
import com.example.livedata.koin.module.viewModelModule
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            applicationContext
            modules(viewModelModule)
        }
    }

}